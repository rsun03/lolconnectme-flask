__author__ = 'Robert Sun'
from models import *
from application import db

'''
Creates the static tables that the database needs
'''


def initialize_static_tables():
    create_gender_ref_table()
    create_league_rank_table()
    create_league_region_table()


def create_gender_ref_table():
    GenderRef.query.delete()
    result_list = [GenderRef('Male'), GenderRef('Female')]
    for result in result_list:
        db.session.add(result)
    db.session.commit()


def create_league_rank_table():
    LeagueRank.query.delete()
    result_list = [LeagueRank('Unranked'), LeagueRank('Bronze'), LeagueRank('Silver'), LeagueRank('Gold'),
                   LeagueRank('Platinum'), LeagueRank('Diamond'), LeagueRank('Master'), LeagueRank('Challenger')]
    for result in result_list:
        db.session.add(result)
    db.session.commit()


def create_league_region_table():
    LeagueRegion.query.delete()
    result_list = (LeagueRegion('North America'), LeagueRegion('EU West'), LeagueRegion('EU Nordic & East'),
                   LeagueRegion('Korea'), LeagueRegion('Latin America North'), LeagueRegion('Latin America South'),
                   LeagueRegion('Brazil'), LeagueRegion('Oceania'), LeagueRegion('Russia'),
                   LeagueRegion('Turkey'))
    for result in result_list:
        db.session.add(result)
    db.session.commit()


initialize_static_tables()
