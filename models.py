from werkzeug.security import generate_password_hash, check_password_hash
from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy()
__author__ = 'Robert Sun'

class User(db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(100), primary_key=True)
    email = db.Column(db.String(255), unique=True)
    pw_hash = db.Column(db.String(255))
    first_name = db.Column(db.String(100))
    last_name = db.Column(db.String(100))
    date_of_birth = db.Column(db.Date)
    profile_picture = db.Column(db.String(2083), nullable=True)
    gender = db.Column(db.Integer, db.ForeignKey('gender_ref.id'))
    user_location = db.relationship('UserLocation', backref='user', lazy='dynamic')
    league_profiles = db.relationship('LeagueProfile', backref='user', lazy='dynamic')

    def __init__(self, username, email, password, first_name, last_name, date_of_birth, profile_picture, gender):
        self.username = username
        self.email = email
        self.set_password(password)
        self.first_name = first_name
        self.last_name = last_name
        self.date_of_birth = date_of_birth
        self.profile_picture = profile_picture
        self.gender = gender

    def set_password(self, password):
        self.pw_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.pw_hash, password)


class GenderRef(db.Model):
    __tablename__ = 'gender_ref'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    gender = db.Column(db.String(10))
    user_profile = db.relationship('User', backref='gender_ref', lazy='dynamic')

    def __init__(self, gender):
        self.gender = gender


class UserLocation(db.Model):
    __tablename__ = 'user_location'
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key=True)
    lat = db.Column(db.Float(precision=32))
    lng = db.Column(db.Float(precision=32))

    def __init__(self, user_id, lat, lng):
        self.user_id = user_id
        self.lat = lat
        self.lng = lng


class LeagueRegion(db.Model):
    __tablename__ = 'league_region'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    region = db.Column(db.String(50))
    league_profile = db.relationship('LeagueProfile', backref='league_region', lazy='dynamic')

    def __init__(self, region):
        self.region = region


class LeagueRank(db.Model):
    __tablename__ = 'league_rank'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    rank = db.Column(db.String(50))
    league_profile = db.relationship('LeagueProfile', backref='league_rank', lazy='dynamic')

    def __init__(self, rank):
        self.rank = rank


class LeagueProfile(db.Model):
    __tablename__ = 'league_profile'
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key=True)
    region = db.Column(db.Integer, db.ForeignKey('league_region.id'))
    rank = db.Column(db.Integer, db.ForeignKey('league_rank.id'))
    summoner_name = db.Column(db.String(100))
    plays_summoners_rift = db.Column(db.Boolean)
    plays_twisted_treeline = db.Column(db.Boolean)
    plays_aram = db.Column(db.Boolean)
    plays_dominion = db.Column(db.Boolean)
    plays_normal_games = db.Column(db.Boolean)
    plays_ranked_games = db.Column(db.Boolean)
    plays_top = db.Column(db.Boolean)
    plays_jungle = db.Column(db.Boolean)
    plays_mid = db.Column(db.Boolean)
    plays_adc = db.Column(db.Boolean)
    plays_support = db.Column(db.Boolean)
    search_for_top = db.Column(db.Boolean)
    search_for_jungle = db.Column(db.Boolean)
    search_for_mid = db.Column(db.Boolean)
    search_for_adc = db.Column(db.Boolean)
    search_for_support = db.Column(db.Boolean)

    def __init__(self, user_id, region, rank, summoner_name, plays_summoners_rift, plays_twisted_treeline,
                 plays_aram, plays_dominion, plays_normal_games, plays_ranked_games, plays_top, plays_jungle, plays_mid,
                 plays_adc, plays_support, search_for_top, search_for_jungle, search_for_mid,
                 search_for_adc, search_for_support):
        self.user_id = user_id
        self.region = region
        self.rank = rank
        self.summoner_name = summoner_name
        self.plays_summoners_rift = plays_summoners_rift
        self.plays_twisted_treeline = plays_twisted_treeline
        self.plays_aram = plays_aram
        self.plays_dominion = plays_dominion
        self.plays_normal_games = plays_normal_games
        self.plays_ranked_games = plays_ranked_games
        self.plays_top = plays_top
        self.plays_jungle = plays_jungle
        self.plays_mid = plays_mid
        self.plays_adc = plays_adc
        self.plays_support = plays_support
        self.search_for_top = search_for_top
        self.search_for_jungle = search_for_jungle
        self.search_for_mid = search_for_mid
        self.search_for_adc = search_for_adc
        self.search_for_support = search_for_support


if __name__ == "__main__":
    db.drop_all()
    db.create_all()
