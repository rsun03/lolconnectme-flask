from flask_restful import abort, reqparse
import flask_restful
from flask import jsonify
from models import *
from flask.ext.httpauth import HTTPBasicAuth
from datetime import date
import requests

auth = HTTPBasicAuth()
riot_games_api_key = '3d1f330a-1b8e-4e33-a215-8cc43ba42134'

users = {
    "admin": "7tmp3cMmzSGC6dve"
}


@auth.get_password
def get_pw(username):
    if username in users:
        return users.get(username)
    return None


'''
Class that handles the API requests
'''

# Requires authentication for all resources
class Resource(flask_restful.Resource):
    method_decorators = [auth.login_required]  # applies to all inherited resources


# API for the User table
class UserAPI(Resource):
    def __init__(self):
        # Initializes the parser for the form arguments
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('email', type=str)
        self.reqparse.add_argument('password', type=str)
        self.reqparse.add_argument('first_name', type=str)
        self.reqparse.add_argument('last_name', type=str)
        self.reqparse.add_argument('date_of_birth', type=str)
        self.reqparse.add_argument('gender', type=int)
        self.reqparse.add_argument('profile_picture', type=str)
        super(UserAPI, self).__init__()

    def get(self, username):
        return get_user(username)

    def put(self, username):
        args = self.reqparse.parse_args()
        email = args['email']
        password = args['password']
        first_name = args['first_name']
        last_name = args['last_name']
        date_of_birth = args['date_of_birth']
        profile_picture = args['profile_picture']
        gender = args['gender']
        return update_user(username, email, password, first_name, last_name, date_of_birth, profile_picture, gender)

    def delete(self, username):
        return delete_user(username)


class UserListAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('username', type=str, required=True)
        self.reqparse.add_argument('email', type=str, required=True)
        self.reqparse.add_argument('password', type=str, required=True)
        self.reqparse.add_argument('first_name', type=str, required=True)
        self.reqparse.add_argument('last_name', type=str, required=True)
        self.reqparse.add_argument('date_of_birth', type=str, required=True)
        self.reqparse.add_argument('gender', type=str, required=True)
        super(UserListAPI, self).__init__()

    def get(self):
        return get_all_users()

    def post(self):
        args = self.reqparse.parse_args()
        username = args['username']
        email = args['email']
        password = args['password']
        first_name = args['first_name']
        last_name = args['last_name']
        date_of_birth = args['date_of_birth']
        gender = args['gender']
        return create_user(username, email, password, first_name, last_name, date_of_birth, gender)


class UserLoginAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('username', type=str, required=True)
        self.reqparse.add_argument('password', type=str, required=True)
        super(UserLoginAPI, self).__init__()

    def post(self):
        args = self.reqparse.parse_args()
        username = args['username']
        password = args['password']
        return check_login(username, password)


class LeagueProfileAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('region', type=int, required=True)
        self.reqparse.add_argument('summoner_name', type=str, required=True)
        self.reqparse.add_argument('plays_summoners_rift', type=int, required=True)
        self.reqparse.add_argument('plays_twisted_treeline', type=int, required=True)
        self.reqparse.add_argument('plays_aram', type=int, required=True)
        self.reqparse.add_argument('plays_dominion', type=int, required=True)
        self.reqparse.add_argument('plays_normal_games', type=int, required=True)
        self.reqparse.add_argument('plays_ranked_games', type=int, required=True)
        self.reqparse.add_argument('plays_top', type=int, required=True)
        self.reqparse.add_argument('plays_jungle', type=int, required=True)
        self.reqparse.add_argument('plays_mid', type=int, required=True)
        self.reqparse.add_argument('plays_adc', type=int, required=True)
        self.reqparse.add_argument('plays_support', type=int, required=True)
        self.reqparse.add_argument('search_for_top', type=int, required=True)
        self.reqparse.add_argument('search_for_jungle', type=int, required=True)
        self.reqparse.add_argument('search_for_mid', type=int, required=True)
        self.reqparse.add_argument('search_for_adc', type=int, required=True)
        self.reqparse.add_argument('search_for_support', type=int, required=True)
        super(LeagueProfileAPI, self).__init__()

    def get(self, id):
        return get_league_profile(id)

    def put(self, id):
        args = self.reqparse.parse_args()
        region = args['region']
        summoner_name = args['summoner_name']
        plays_summoners_rift = args['plays_summoners_rift']
        plays_twisted_treeline = args['plays_twisted_treeline']
        plays_aram = args['plays_aram']
        plays_dominion = args['plays_dominion']
        plays_normal_games = args['plays_normal_games']
        plays_ranked_games = args['plays_ranked_games']
        plays_top = args['plays_top']
        plays_jungle = args['plays_jungle']
        plays_mid = args['plays_mid']
        plays_adc = args['plays_adc']
        plays_support = args['plays_support']
        search_for_top = args['search_for_top']
        search_for_jungle = args['search_for_jungle']
        search_for_mid = args['search_for_mid']
        search_for_adc = args['search_for_adc']
        search_for_support = args['search_for_support']
        return update_league_profile(id, region, summoner_name, plays_summoners_rift, plays_twisted_treeline,
                                     plays_aram, plays_dominion, plays_normal_games, plays_ranked_games, plays_top,
                                     plays_jungle,
                                     plays_mid,
                                     plays_adc, plays_support, search_for_top,
                                     search_for_jungle, search_for_mid, search_for_adc, search_for_support)

    def delete(self, id):
        return delete_league_profile(id)


class LeagueProfileListAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('user_id', type=int, required=True)
        self.reqparse.add_argument('region', type=int, required=True)
        self.reqparse.add_argument('summoner_name', type=str, required=True)
        self.reqparse.add_argument('plays_summoners_rift', type=int, required=True)
        self.reqparse.add_argument('plays_twisted_treeline', type=int, required=True)
        self.reqparse.add_argument('plays_aram', type=int, required=True)
        self.reqparse.add_argument('plays_dominion', type=int, required=True)
        self.reqparse.add_argument('plays_normal_games', type=int, required=True)
        self.reqparse.add_argument('plays_ranked_games', type=int, required=True)
        self.reqparse.add_argument('plays_top', type=int, required=True)
        self.reqparse.add_argument('plays_jungle', type=int, required=True)
        self.reqparse.add_argument('plays_mid', type=int, required=True)
        self.reqparse.add_argument('plays_adc', type=int, required=True)
        self.reqparse.add_argument('plays_support', type=int, required=True)
        self.reqparse.add_argument('search_for_top', type=int, required=True)
        self.reqparse.add_argument('search_for_jungle', type=int, required=True)
        self.reqparse.add_argument('search_for_mid', type=int, required=True)
        self.reqparse.add_argument('search_for_adc', type=int, required=True)
        self.reqparse.add_argument('search_for_support', type=int, required=True)
        super(LeagueProfileListAPI, self).__init__()

    def get(self):
        return get_all_league_profiles()

    def post(self):
        args = self.reqparse.parse_args()
        user_id = args['user_id']
        region = args['region']
        summoner_name = args['summoner_name']
        plays_summoners_rift = args['plays_summoners_rift']
        plays_twisted_treeline = args['plays_twisted_treeline']
        plays_aram = args['plays_aram']
        plays_dominion = args['plays_dominion']
        plays_normal_games = args['plays_normal_games']
        plays_ranked_games = args['plays_ranked_games']
        plays_top = args['plays_top']
        plays_jungle = args['plays_jungle']
        plays_mid = args['plays_mid']
        plays_adc = args['plays_adc']
        plays_support = args['plays_support']
        search_for_top = args['search_for_top']
        search_for_jungle = args['search_for_jungle']
        search_for_mid = args['search_for_mid']
        search_for_adc = args['search_for_adc']
        search_for_support = args['search_for_support']
        return create_league_profile(user_id, region, summoner_name, plays_summoners_rift, plays_twisted_treeline,
                                     plays_aram, plays_dominion, plays_normal_games, plays_ranked_games, plays_top,
                                     plays_jungle,
                                     plays_mid,
                                     plays_adc, plays_support, search_for_top,
                                     search_for_jungle, search_for_mid, search_for_adc, search_for_support)


class UserPasswordAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('current_password', type=str, required=True)
        self.reqparse.add_argument('new_password', type=str, required=True)

    def put(self, username):
        args = self.reqparse.parse_args()
        current_password = args['current_password']
        new_password = args['new_password']
        return change_user_password(username, current_password, new_password)


class SearchAPI(Resource):
    def __init__(self):
        super(SearchAPI, self).__init__()

    def get(self, id):
        return get_search_list(id)


# Returns the user information in a dict, given the id
def get_user(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        abort(400, message="User does not exist")
    result_list = {'id': user.id, 'username': user.username,
                   'email': user.email,
                   'firstName': user.first_name, 'lastName': user.last_name,
                   'dateOfBirth': str(user.date_of_birth),
                   'profilePicture': user.profile_picture,
                   'gender': user.gender_ref.gender}
    return jsonify(result_list)


# Updates user row
def update_user(username, email, password, first_name, last_name, date_of_birth, profile_picture, gender):
    user = User.query.filter_by(username=username).first()
    if user is None:
        abort(400, message="User does not exist")
    else:
        # try:
        user.email = email if email is not None else user.email
        user.first_name = first_name.capitalize() if first_name is not None else user.first_name
        user.last_name = last_name.capitalize() if last_name is not None else user.last_name
        user.date_of_birth = date_of_birth if date_of_birth is not None else user.date_of_birth
        user.profile_picture = profile_picture if profile_picture is not None else user.profile_picture
        user.gender = gender if gender is not None else user.gender
        db.session.commit()
        result = {'id': user.id, 'username': user.username, 'email': user.email, 'firstName': user.first_name,
                  'lastName': user.last_name, 'dateOfBirth': str(user.date_of_birth),
                  'message': 'Profile updated successfully', 'gender': user.gender_ref.gender}
        return jsonify(result)
        # except:
        #     session.rollback()
        #     raise


# Deletes User row
def delete_user(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        abort(400, message="User does not exist")
    else:
        # try:
        db.session.delete(user)
        db.session.commit()

        return message_response("User deleted successfully")
        # except:
        #     session.rollback()
        #     raise


def change_user_password(username, current_password, new_password):
    user = User.query.filter_by(username=username).first()
    if user is None:
        abort(400, message="User does not exist")
    if user.check_password(current_password):
        user.pw_hash = generate_password_hash(new_password)
        db.session.commit()
        return message_response("Password changed successfully")
    else:
        abort(400, message="Incorrect password")


# Returns a dict of all the user profiles
def get_all_users():
    users = User.query.all()
    result_list = []
    for user in users:
        result = {'id': user.id, 'username': user.username,
                  'email': user.email,
                  'firstName': user.first_name, 'lastName': user.last_name,
                  'dateOfBirth': str(user.date_of_birth),
                  'profilePicture': user.profile_picture,
                  'gender': user.gender_ref.gender}
        result_list.append(result)
    return jsonify({'data': result_list})


def create_user(username, email, password, first_name, last_name, date_of_birth, gender):
    if User.query.filter_by(username=username).first() is not None:
        abort(400, message="That username is already registered")
    elif User.query.filter_by(email=email).first() is not None:
        abort(400, message="That email address is already registered")
    else:
        u = User(username, email, password, first_name.capitalize(), last_name.capitalize(), date_of_birth, None,
                 gender)
        # try:
        db.session.add(u)
        db.session.commit()

        # except:
        #     session.rollback()
        #     raise
        user = User.query.filter_by(username=username).first()
        if user is None:
            abort(400, message="An unknown error occurred")
        result = {'id': user.id, 'username': user.username, 'email': user.email, 'firstName': user.first_name,
                  'lastName': user.last_name, 'dateOfBirth': str(user.date_of_birth),
                  'message': 'User registered successfully', 'gender': user.gender_ref.gender}
        return jsonify(result)


def create_league_profile(user_id, region, summoner_name, plays_summoners_rift, plays_twisted_treeline,
                          plays_aram, plays_dominion,
                          plays_normal_games, plays_ranked_games,
                          plays_top, plays_jungle, plays_mid, plays_adc, plays_support,
                          search_for_top, search_for_jungle, search_for_mid, search_for_adc,
                          search_for_support):
    if User.query.filter_by(id=user_id).first() is None:
        abort(400, message="User does not exist")
    if LeagueProfile.query.filter_by(user_id=user_id).first() is not None:
        abort(400, message="User already has a league profile")
    else:
        tier = get_rank_by_summoner_name(summoner_name)
        tier_num = LeagueRank.query.filter_by(rank=tier).first().id
        u = LeagueProfile(user_id, region, tier_num, summoner_name, plays_summoners_rift, plays_twisted_treeline,
                          plays_aram, plays_dominion,
                          plays_normal_games, plays_ranked_games, plays_top, plays_jungle, plays_mid, plays_adc,
                          plays_support,
                          search_for_top, search_for_jungle, search_for_mid, search_for_adc,
                          search_for_support)
        db.session.add(u)
        db.session.commit()
        return message_response("League Profile created successfully")


def update_league_profile(user_id, region, summoner_name, plays_summoners_rift, plays_twisted_treeline,
                          plays_aram, plays_dominion, plays_normal_games, plays_ranked_games, plays_top, plays_jungle,
                          plays_mid,
                          plays_adc,
                          plays_support, search_for_top, search_for_jungle,
                          search_for_mid, search_for_adc, search_for_support):
    league_profile = LeagueProfile.query.filter_by(user_id=user_id).first()
    if league_profile is None:
        return create_league_profile(user_id, region, summoner_name, plays_summoners_rift, plays_twisted_treeline,
                                     plays_aram, plays_dominion,
                                     plays_normal_games, plays_ranked_games,
                                     plays_top, plays_jungle, plays_mid, plays_adc, plays_support,
                                     search_for_top, search_for_jungle, search_for_mid, search_for_adc,
                                     search_for_support)
    else:
        tier = get_rank_by_summoner_name(summoner_name)
        tier_num = LeagueRank.query.filter_by(rank=tier).first().id
        league_profile.region = region if region is not None else league_profile.region
        league_profile.rank = tier_num if tier_num is not None else league_profile.rank
        league_profile.summoner_name = summoner_name if summoner_name is not None else league_profile.summoner_name
        league_profile.plays_summoners_rift = plays_summoners_rift if plays_summoners_rift is not None else league_profile.plays_summoners_rift
        league_profile.plays_twisted_treeline = plays_twisted_treeline if plays_twisted_treeline is not None else league_profile.plays_twisted_treeline
        league_profile.plays_aram = plays_aram if plays_aram is not None else league_profile.plays_aram
        league_profile.plays_dominion = plays_dominion if plays_dominion is not None else league_profile.plays_dominion
        league_profile.plays_normal_games = plays_normal_games if plays_normal_games is not None else league_profile.plays_normal_games
        league_profile.plays_ranked_games = plays_ranked_games if plays_ranked_games is not None else league_profile.plays_ranked_games
        league_profile.plays_top = plays_top if plays_top is not None else league_profile.plays_top
        league_profile.plays_jungle = plays_jungle if plays_jungle is not None else league_profile.plays_jungle
        league_profile.plays_mid = plays_mid if plays_mid is not None else league_profile.plays_mid
        league_profile.plays_adc = plays_adc if plays_adc is not None else league_profile.plays_adc
        league_profile.plays_support = plays_support if plays_support is not None else league_profile.plays_support
        league_profile.search_for_top = search_for_top if search_for_top is not None else league_profile.search_for_top
        league_profile.search_for_jungle = search_for_jungle if search_for_jungle is not None else league_profile.search_for_jungle
        league_profile.search_for_mid = search_for_mid if search_for_mid is not None else league_profile.search_for_mid
        league_profile.search_for_adc = search_for_adc if search_for_adc is not None else league_profile.search_for_adc
        league_profile.search_for_support = search_for_support if search_for_support is not None else league_profile.search_for_support
        db.session.commit()
        return message_response("League profile updated successfully")


def delete_league_profile(user_id):
    league_profile = LeagueProfile.query.filter_by(user_id=user_id)
    if league_profile is None:
        abort(400, message="League profile does not exist!")
    else:
        db.session.delete(league_profile)
        db.session.commit()
        return message_response("League profile deleted successfully")


def get_league_profile(user_id):
    league_profile = LeagueProfile.query.filter_by(user_id=user_id).first()
    if league_profile is None:
        abort(400, message="League profile does not exist!")
    else:
        result = {'user_id': league_profile.user_id, 'region': league_profile.league_region.region,
                  'rank': league_profile.league_rank.rank, 'summonerName': league_profile.summoner_name,
                  'playsSummonersRift': league_profile.plays_summoners_rift,
                  'playsTwistedTreeline': league_profile.plays_twisted_treeline, 'playsAram': league_profile.plays_aram,
                  'playsDominion': league_profile.plays_dominion,
                  'playsNormalGames': league_profile.plays_normal_games,
                  'playsRankedGames': league_profile.plays_ranked_games,
                  'playsTop': league_profile.plays_top, 'playsJungle': league_profile.plays_jungle,
                  'playsMid': league_profile.plays_mid,
                  'playsAdc': league_profile.plays_adc,
                  'playsSupport': league_profile.plays_support,
                  'searchForTop': league_profile.search_for_top,
                  'searchForJungle': league_profile.search_for_jungle,
                  'searchForMid': league_profile.search_for_mid,
                  'searchForAdc': league_profile.search_for_adc,
                  'searchForSupport': league_profile.search_for_support}
        return jsonify(result)


def get_all_league_profiles():
    league_profiles = LeagueProfile.query.all()
    result_list = []
    for league_profile in league_profiles:
        result = {'user_id': league_profile.user_id, 'region': league_profile.league_region.region,
                  'rank': league_profile.league_rank.rank, 'summonerName': league_profile.summoner_name,
                  'playsSummonersRift': league_profile.plays_summoners_rift,
                  'playsTwistedTreeline': league_profile.plays_twisted_treeline, 'playsAram': league_profile.plays_aram,
                  'playsDominion': league_profile.plays_dominion,
                  'playsNormalGames': league_profile.plays_normal_games,
                  'playsRankedGames': league_profile.plays_ranked_games,
                  'playsTop': league_profile.plays_top, 'playsJungle': league_profile.plays_jungle,
                  'playsMid': league_profile.plays_mid,
                  'playsAdc': league_profile.plays_adc,
                  'playsSupport': league_profile.plays_support,
                  'searchForTop': league_profile.search_for_top,
                  'searchForJungle': league_profile.search_for_jungle,
                  'searchForMid': league_profile.search_for_mid,
                  'searchForAdc': league_profile.search_for_adc,
                  'searchForSupport': league_profile.search_for_support}
        result_list.append(result)
    return jsonify({'data': result_list})


def check_login(username, password):
    user = User.query.filter_by(username=username).first()
    if user is None:
        abort(400, message="Invalid username or password")
    else:
        if check_password_hash(user.pw_hash, password):
            result = {'id': user.id, 'username': user.username, 'email': user.email, 'firstName': user.first_name,
                      'lastName': user.last_name, 'dateOfBirth': str(user.date_of_birth),
                      'message': 'User logged in successfully', 'gender': user.gender_ref.gender}
            return jsonify(result)
        else:
            abort(400, message="Invalid username or password")


def message_response(message):
    return jsonify({'message': message})


# Returns a list of users based on the given user (id's) search preferences)
def get_search_list(user_id):
    user = LeagueProfile.query.filter_by(user_id=user_id).first()
    if user is None:
        abort(400, message="User does not exist")
    exact_matches = LeagueProfile.query \
        .filter(LeagueProfile.region == user.region) \
        .filter(LeagueProfile.user_id != user.user_id) \
        .filter((LeagueProfile.plays_summoners_rift == user.plays_summoners_rift) |
                (LeagueProfile.plays_twisted_treeline == user.plays_twisted_treeline) |
                (LeagueProfile.plays_aram == user.plays_aram) |
                (LeagueProfile.plays_dominion == user.plays_dominion)) \
        .filter((LeagueProfile.plays_normal_games == user.plays_normal_games) |
                (LeagueProfile.plays_ranked_games == user.plays_ranked_games)) \
        .filter((LeagueProfile.plays_top == user.search_for_top) |
                (LeagueProfile.plays_jungle == user.search_for_jungle) |
                (LeagueProfile.plays_mid == user.search_for_mid) |
                (LeagueProfile.plays_adc == user.search_for_adc) |
                (LeagueProfile.plays_support == user.search_for_support)) \
        .filter((LeagueProfile.rank == user.rank - 1) |
                (LeagueProfile.rank == user.rank) |
                (LeagueProfile.rank == user.rank + 1)) \
        .all()
    result_list = []
    for row in exact_matches:
        result_list.append({'user_id': row.user_id, 'firstName': row.user.first_name, 'lastName': row.user.last_name,
                            'rank': row.league_rank.rank, 'summonerName': row.summoner_name,
                            'age': calculate_age(row.user.date_of_birth),
                            'gender': row.user.gender,
                            'playsSummonersRift': row.plays_summoners_rift,
                            'playsTwistedTreeline': row.plays_twisted_treeline, 'playsAram': row.plays_aram,
                            'playsDominion': row.plays_dominion,
                            'playsNormalGames': row.plays_normal_games,
                            'playsRankedGames': row.plays_ranked_games,
                            'playsTop': row.plays_top, 'playsJungle': row.plays_jungle,
                            'playsMid': row.plays_mid,
                            'playsAdc': row.plays_adc,
                            'playsSupport': row.plays_support})
    return jsonify({'data': result_list})


def get_rank_by_summoner_name(summoner_name):
    r = requests.get(
        'https://na.api.pvp.net/api/lol/na/v1.4/summoner/by-name/' + summoner_name + '?api_key=' + riot_games_api_key)
    id = None
    # If user summoner does not exist, just put unranked
    if r.status_code == 404:
        return 'UNRANKED'
    else:
        for value in r.json().values():
            id = value['id']
    r = requests.get(
        'https://na.api.pvp.net/api/lol/na/v2.5/league/by-summoner/' + str(id) + '?api_key=' + riot_games_api_key)
    tier = list(r.json().values())[0][0]['tier']
    return tier


def check_summoner_name_exists(summoner_name):
    r = requests.get(
        'https://na.api.pvp.net/api/lol/na/v1.4/summoner/by-name/' + summoner_name + '?api_key=' + riot_games_api_key)
    # If user summoner does not exist, just put unranked
    return False if r.status_code == 404 else True


def calculate_age(dob):
    today = date.today()
    return today.year - dob.year - ((today.month, today.day) < (dob.month, dob.day))
