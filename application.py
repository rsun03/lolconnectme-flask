#! ../env/Scripts/python
__author__ = 'Robert Sun'
from flask import Flask
from flask_restful import Api
from models import db
from resources import UserAPI, UserListAPI, LeagueProfileAPI, LeagueProfileListAPI, UserLoginAPI, SearchAPI, \
    UserPasswordAPI

errors = {
    'UserAlreadyExistsError': {
        'message': "A user with that username already exists.",
        'status': 409,
    },
    'ResourceDoesNotExist': {
        'message': "A resource with that ID no longer exists.",
        'status': 410,
        'extra': "Any extra information you want.",
    },
    'WrongCredentials': {
        'message': 'Wrong login credentials',
        'status': 409
    }
}

# The endpoints for which to call the api
application = Flask(__name__)
application.config.from_pyfile('config.py')
db.app = application
db.init_app(application)

# Makes the app secure http
# sslify = SSLify(application)
api = Api(application, errors=errors)
api.add_resource(UserAPI, '/users/<string:username>')
api.add_resource(UserListAPI, '/users')
api.add_resource(LeagueProfileAPI, '/users/leagueprofile/<int:id>')
api.add_resource(LeagueProfileListAPI, '/users/leagueprofile')
api.add_resource(UserLoginAPI, '/users/login')
api.add_resource(SearchAPI, '/search/<int:id>')
api.add_resource(UserPasswordAPI, '/users/password/<string:username>')


@application.route('/test/<string:summoner_name>')
def main(summoner_name):
    from resources import get_rank_by_summoner_name
    return get_rank_by_summoner_name(summoner_name)


if __name__ == '__main__':
    application.run()
